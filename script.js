window.addEventListener("load", function () {
  console.log("LOADED");
  //select grid and declare helper variables
  const grid = this.document.querySelector(".grid");
  const label = document.querySelector(".label");
  let width = 10;
  let bombs = 20; //varible to determine the count of bombs
  let flags = 20;
  let squares = [];
  let isGameOver = false;

  function startGame() {
    const bombsArray = Array(bombs).fill("bomb");
    const emptyArray = Array(width * width - bombs).fill("valid");
    const gameArray = [...bombsArray, ...emptyArray];
    const shuffledArray = gameArray.sort((a, b) => Math.random() - 0.5);

    //create squares and append them to the grid
    shuffledArray.forEach((item, index) => {
      const square = document.createElement("div");
      square.setAttribute("id", index);
      square.classList.add(item);
      square.addEventListener("click", (e) => {
        click(e.target);
      });
      square.addEventListener("contextmenu", (e) => {
        e.preventDefault();
        if (!e.target.classList.contains("flag"))
          e.target.classList.add("flag");
        else return;

        if (e.target.classList.contains("bomb")) {
          gameOver();
          label.innerHTML = "You lose";
          grid.classList.add("over");
        }
      });
      grid.appendChild(square);
      squares.push(square);
    });

    squares.forEach((item, index) => {
      let total = 0;
      let isRight = index % width === width - 1;
      let isLeft = index % width === 0;
      if (index > 0 && !isLeft && squares[index - 1].classList.contains("bomb"))
        total++;
      if (index > 9 && squares[index - width].classList.contains("bomb"))
        total++;
      if (index < 90 && squares[index + width].classList.contains("bomb"))
        total++;
      if (
        index < 99 &&
        !isRight &&
        squares[index + 1].classList.contains("bomb")
      )
        total++;
      if (
        index > 10 &&
        !isLeft &&
        squares[index - width - 1].classList.contains("bomb")
      )
        total++;
      if (
        index > 9 &&
        !isRight &&
        squares[index - width + 1].classList.contains("bomb")
      )
        total++;
      if (
        index < 89 &&
        !isRight &&
        squares[index + width + 1].classList.contains("bomb")
      )
        total++;
      if (
        index < 89 &&
        !isLeft &&
        squares[index + width - 1].classList.contains("bomb")
      )
        total++;
      item.setAttribute("data", total);
    });
  }

  startGame();

  function click(square) {
    let currentId = square.getAttribute("id");
    let total = square.getAttribute("data");
    if (isGameOver) return;
    if (
      square.classList.contains("checked") ||
      square.classList.contains("flag")
    )
      return;

    if (square.classList.contains("bomb")) {
      gameOver(square);
    } else {
      if (total != 0) {
        square.classList.add("checked");
        square.innerText = total;
        return;
      }
      checkSquares(square, currentId);
    }
    square.classList.add("checked");
  }

  function checkSquares(square, id) {
    const isLeft = id % width === 0;
    const isRight = id % width === width - 1;

    setTimeout(() => {
      if (id > 0 && !isLeft) {
        const newId = squares[parseInt(id) - 1].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id > 9) {
        const newId = squares[parseInt(id) - width].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id < 90) {
        const newId = squares[parseInt(id) + width].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id < 99 && !isRight) {
        const newId = squares[parseInt(id) + 1].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id > 10 && !isLeft) {
        const newId = squares[parseInt(id) - width - 1].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id > 9 && !isRight) {
        const newId = squares[parseInt(id) - width + 1].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id < 89 && !isRight) {
        const newId = squares[parseInt(id) + width + 1].id;
        const element = document.getElementById(newId);
        click(element);
      }

      if (id < 89 && !isLeft) {
        const newId = squares[parseInt(id) + width - 1].id;
        const element = document.getElementById(newId);
        click(element);
      }
    }, 10);
  }

  function gameOver(square) {
    isGameOver = true;
    label.innerHTML = "You lose";
    grid.classList.add("over");
    squares.forEach((square) => {
      if (square.classList.contains("bomb")) square.classList.add("show");
    });
  }
});
